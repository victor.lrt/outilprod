# Outils de production
## TP0 : Les commandes de base

Lien : https://gitlab.com/victor.lrt/outilprod

mkdir LARMET_victor_TP01
cd LARMET_victor_TP01
touch index.html 

git init
Output : Initialized empty Git repository in C:/Users/victo/Desktop/CNAM/1A/OutilProd/TP/LARMET_victor_TP01/.git/

git config --global user.name "Victor Larmet"
git config --global user.email "victor.larmet@gmail.com"
git config --global user.username "victor.lrt"

git status
Output : Untracked files:
  (use "git add <file>..." to include in what will be committed)
        index.html

git add *

git status

git commit -m "feat: ajoute le fichier index.html"

git status

Questions

1. Non car un commit a un identifiant unique (attribué par une fonction de hachage comme SHA-1).
2. Oui à l'aide de la commande "git add *".
3. Non ce n'est pas possible, il serait nécessaire de le déplacer dans l'arborescence de travail.
4. Un VCS est indispensable dans tout developpement logiciel mais il peut être aussi utilisée pour d'autres activités afin de maintenir un historique de version et un suivi des collaborateurs.
5. Oui il est possible de le supprimer grâce à la commande "git rm [fichier]" à la suite d'un "git add".

## TP1 : Les branches

Liste les branches : git branch -v
Equivalent de cmd branch, switch et push set upstream git checkout -b [nomBranche] 

Ajout de la première banche : git branch ajout-styles
Changer de branche (commit au préalable) : git switch ajout-styles

git commit -m "feat(styles) : add file css and blue class to html"
[ajout-styles c1b030d] feat(styles) : add file css and blue class to html
 2 files changed, 16 insertions(+)
 create mode 100644 index.html
 create mode 100644 style.css

Crée et change de branche : git switch -c modification-textes
Modification du paragraphe puis git add *
git commit -m "feat(text) : update text paragraph"
[modification-textes d805cff] feat(text) : update text paragraph
 1 file changed, 1 insertion(+), 1 deletion(-)

git switch ajout-styles
Switched to branch 'ajout-styles'
git push -u origin ajout-styles

git switch main
Switched to branch 'main'
Your branch is ahead of 'origin/main' by 2 commits.
  (use "git push" to publish your local commits)

git merge modification-textes
Updating 14cccd8..c9bba18
Fast-forward
 index.html | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)

 git merge ajout-styles
CONFLICT (add/add): Merge conflict in index.html
Auto-merging index.html
Automatic merge failed; fix conflicts and then commit the result.

Questions

1. Fas-forward permet de faire un merge sans commit
2. Nous avons essayé de modifier la meme ligne du fichier dans deux branches différentes
3. Si la branche est sur la forge il est possible de la recuperer
4. Le graph est explicite
